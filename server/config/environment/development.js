'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 	process.env.MONGO_PORT_27017_TCP_ADDR ||
		'mongodb://localhost/groween-dev'
  },

  seedDB: true,

  gcm_auth_key: 'AIzaSyC2eu9c4u6XdwoAomBR9alNs32AbRvLbgQ'
};
