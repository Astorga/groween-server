'use strict';

// Test specific configuration
// ===========================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/groween-test'
  },

  gcm_auth_key: 'AIzaSyC2eu9c4u6XdwoAomBR9alNs32AbRvLbgQ'
};