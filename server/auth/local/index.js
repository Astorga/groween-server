'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var User = require('../../api/user/user.model');
var SubscriptionType = require('../../api/subscriptionType/subscriptionType.model');
var Profession = require('../../api/profession/profession.model');

var router = express.Router();

router.post('/', function(req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    var error = err || info;
    if (error) return res.json(401, error);
    if (!user) return res.json(404, {message: 'Something went wrong, please try again.'});

    User.populate(user, {
      path: 'profile.profession',
      model: Profession
    }, function(err, userPopulated){
      if(err) {
        res.status(500).json(err);
      }
      User.populate(userPopulated, {
        path: 'profile.subscriptionType',
        model: SubscriptionType
      }, function(err, userMorePopulated){
        if(err) {
          res.status(500).json(err);
        }
        var token = auth.signToken(userMorePopulated._id, userMorePopulated.role);
        res.json({
          token: token,
          info: userMorePopulated.info
        });
      });
    });

    /*var token = auth.signToken(user._id, user.role);
    res.json({
      token: token,
      info: user.info
    });*/
  })(req, res, next)
});

module.exports = router;