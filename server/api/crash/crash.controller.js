'use strict';

var _ = require('lodash');
var Crash = require('./crash.model');
var mongoose = require('mongoose');

// Get list of reinforceContents
exports.index = function(req, res) {
  Crash
  .find()
  .sort('created_at')
  .exec(function (err, reinforceContents) {
    if(err) { 
      return handleError(res, err); 
    }
    return res.status(200).json(reinforceContents);
  });
};

// Get a single reinforceContent
exports.show = function(req, res) {
  Crash
  .findOne({ _id: req.params.id })
  .exec(function (err, reinforceContent) {
    if(err) { return handleError(res, err); }
    if(!reinforceContent) { return res.status(404).json(); }
    return res.status(200).json(reinforceContent);
  });
};

// Creates a new reinforceContent in the DB.
exports.create = function(req, res) {
  Crash
  .create(req.body, function(err, reinforceContent) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(reinforceContent);
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}