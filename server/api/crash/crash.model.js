'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CrashSchema = new Schema({
  ANDROID_VERSION: 			{ type: String }
  , APP_VERSION_CODE: 		{ type: String }
  , APP_VERSION_NAME: 		{ type: String }
  , AVAILABLE_MEM_SIZE: 	{ type: String }
  , BRAND: 					{ type: String }
  , BUILD: 					{ type: String }
  , CRASH_CONFIGURATION: 	{ type: String }
  , CUSTOM_DATA: 		 	{ type: String }
  , DEVICE_FEATURES: 		{ type: String }
  , DISPLAY: 		 		{ type: String }
  , DUMPSYS_MEMINFO:  		{ type: String }
  , ENVIROMENT: 	 		{ type: String }
  , FILE_PATH: 		 		{ type: String }
  , INITIAL_CONFIGURATION: 	{ type: String }
  , INSTALLATION_ID:  		{ type: String }
  , IS_SILENT: 		 		{ type: String }
  , LOGCAT: 		 		{ type: String }
  , PACKAGE_NAME: 	 		{ type: String }
  , PHONE_MODEL: 	 		{ type: String }
  , PRODUCT: 	 			{ type: String }
  , SETTINGS_GLOBAL: 	 	{ type: String }
  , SETTINGS_SECURE: 	 	{ type: String }
  , SETTINGS_SYSTEM: 	 	{ type: String }
  , SHARED_PREFERENCES: 	{ type: String }
  , STACK_TRACE: 	 		{ type: String }
  , TOTAL_MEM_SIZE: 	 	{ type: String }
  , USER_EMAIL: 	 		{ type: String }
  , created_at: 			{ type: Number }
  , updated_at: 			{ type: Number }
});

CrashSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('Crash', CrashSchema);