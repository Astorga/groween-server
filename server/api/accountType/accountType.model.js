'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AccountTypeSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
});

AccountTypeSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('AccountType', AccountTypeSchema);