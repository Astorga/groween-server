'use strict';

var _ = require('lodash');
var ResourceType = require('./resourceType.model');

// Get list of resourceTypes
exports.index = function(req, res) {
  ResourceType.find(function (err, resourceTypes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(resourceTypes);
  });
};

// Get a single resourceType
exports.show = function(req, res) {
  ResourceType.findById(req.params.id, function (err, resourceType) {
    if(err) { return handleError(res, err); }
    if(!resourceType) { return res.status(404).json(); }
    return res.json(resourceType);
  });
};

// Creates a new resourceType in the DB.
exports.create = function(req, res) {
  ResourceType.create(req.body, function(err, resourceType) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(resourceType);
  });
};

// Updates an existing resourceType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ResourceType.findById(req.params.id, function (err, resourceType) {
    if (err) { return handleError(res, err); }
    if(!resourceType) { return res.status(404); }
    var updated = _.merge(resourceType, req.body);
    if(req.body.tops){
      updated.tops = req.body.tops;
      updated.markModified('tops');
    }
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(resourceType);
    });
  });
};

// Deletes a resourceType from the DB.
exports.destroy = function(req, res) {
  ResourceType.findById(req.params.id, function (err, resourceType) {
    if(err) { return handleError(res, err); }
    if(!resourceType) { return res.status(404).json(); }
    resourceType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}