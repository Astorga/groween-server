'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SubscriptionTypeSchema = new Schema({
  name: 				{ type: String, unique: true, trim: true, required: true }
  , numberOfActives: 	{ type: Number, required: true }
  , hasPubli: 			{ type: Boolean, default: true }
  , created_at: 		{ type: Number }
  , updated_at: 		{ type: Number }
});

SubscriptionTypeSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('SubscriptionType', SubscriptionTypeSchema);