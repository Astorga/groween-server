'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/me/totalDonation', auth.isAuthenticated(), controller.myDonation);
router.put('/me', auth.isAuthenticated(), controller.update);
router.delete('/me', auth.isAuthenticated(), controller.delete);
router.post('/registrationId', auth.isAuthenticated(), controller.setRegistrationId);
router.post('/testGCM', auth.isAuthenticated(), controller.testGCM);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.post('/refresh', controller.refresh);
router.post('/resetPassword', controller.resetPassword); //Post params: email, refreshPasswordToken, password | Response: user object
router.post('/requestResetPassword', controller.requestResetPassword) //Post params: email | response: 200, 404, 500

module.exports = router;
