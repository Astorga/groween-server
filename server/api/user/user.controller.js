'use strict';

var User = require('./user.model');
var QuestionaryAnswer = require('../questionaryAnswer/questionaryAnswer.model');
var SubscriptionType = require('../subscriptionType/subscriptionType.model');
var Profession = require('../profession/profession.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var GCM = require('../../gcm/gcm.controller');
var _ = require('lodash');

var validationError = function(res, err) {
  return res.status(422).json({ subcode:107, message: "Email in use" });
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.status(500).json(err);
    res.status(200).json(users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';

  SubscriptionType.findOne({'name': 'basic'}).exec(function(err, subscriptionType){
    if(err){
      res.status(500).json(err);
    }
    newUser.profile.subscriptionType = subscriptionType;
    newUser.save(function(err, user) {
      if (err) {
        return validationError(res, err);
      }
      
      User.populate(user, {
        path: 'profile.profession',
        model: Profession
      }, function(err, userPopulated){
        if(err) {
          res.status(500).json(err);
        }
        User.populate(userPopulated, {
          path: 'profile.subscriptionType',
          model: SubscriptionType
        }, function(err, userMorePopulated){
          if(err) {
            res.status(500).json(err);
          }
          var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: config.expirationTime });
          res.json({
            token: token,
            info: userMorePopulated.info
          });
        });
      });
    });
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;
  User
  .findOne({"_id": userId})
  .exec(function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).json({ subcode:106, message: "User doesn't exist" });
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.status(500).json(err);
    return res.status(204).json();
  });
};

// Updates self user
exports.update = function(req, res) {
  /*
  if(req.body._id) { delete req.body._id; }
  if(req.body.salt) { delete req.body.salt; }
  if(req.body.hashedPassword) { delete req.body.hashedPassword; }
  if(req.body._password) { delete req.body._password; }
  if(req.body.refreshToken) { delete req.body.refreshToken; }
  if(req.body.role) { delete req.body.role; }
  if(req.body.provider) { delete req.body.provider; }
  if(req.body.gcm) { delete req.body.gcm; }
  if(req.body.facebook) { delete req.body.facebook; }
  if(req.body.twitter) { delete req.body.twitter; }
  if(req.body.google) { delete req.body.google; }
  if(req.body.github) { delete req.body.github; }

  if (req.body.profile) {
    if(req.body.profile.questionaries) { delete req.body.profile.questionaries; }
    if(req.body.profile.subscriptionType) { delete req.body.profile.subscriptionType; }
  }
  */

  var body = {};
  if (req.body.password) {
    body.password = req.body.password;
  };
  var profile = {};
  if (req.body.name) { profile.name = req.body.name };
  if (req.body.lastname) { profile.lastname = req.body.lastname };
  if (req.body.birthdate) { profile.birthdate = req.body.birthdate };
  if (req.body.userSex) { profile.userSex = req.body.userSex };
  if (req.body.country) { profile.country = req.body.country };
  if (req.body.pc) { profile.pc = req.body.pc };
  if (req.body.studyLevel) { profile.studyLevel = req.body.studyLevel };
  if (req.body.profession) { profile.profession = req.body.profession };
  if (req.body.categories) {
    if (typeof req.body.categories == 'string') {
      req.body.categories = req.body.categories.split(" ").join("");
      req.body.categories = req.body.categories.split("]").join("");
      req.body.categories = req.body.categories.split("[").join("");
      req.body.categories = req.body.categories.split(",");
    }
    profile.categories = req.body.categories;
  };

  var userId = req.user._id;
  User
    .findOne({_id: userId})
    .select('')
    .populate('profile.subscriptionType profile.profession')
    .exec(function(err, user) { // don't ever give out the password or salt
      if (err) { return res.status(500).json(err); }
      if(!user) { return res.status(404).json(); }
      console.log('merging user');
      _.merge(user.profile, profile);
      _.merge(user, body);
      user.markModified('profile.categories');

      user.save(function (err, wee) {
        if (err) { return res.status(500).json(err); }
        user.salt = undefined;
        user.hashedPassword = undefined;
        User.populate(user, {
          path: 'profile.profession',
          model: Profession
        }, function(err, userPopulated){
          if(err) {
            res.status(500).json(err);
          }
          User.populate(userPopulated, {
            path: 'profile.subscriptionType',
            model: SubscriptionType
          }, function(err, userMorePopulated){
            if(err) {
              res.status(500).json(err);
            }
            res.status(200).json(user);
          });
        });
      });
    });
};

// Delete self user
exports.delete = function(req, res) {
  var userId = req.user._id;
  User.findByIdAndRemove(userId, function(err, user) {
    if(err) return res.status(500).json(err);
    return res.status(204).json();
  });
};

/**
 * Set new user RegistrationId
 */
exports.setRegistrationId = function(req, res, next) {
  User.findById(req.user._id, function (err, user) {
    if(err){
      console.log("returning 500");
      return res.status(500).json();
    }
    if(user.gcm.registrationIds.indexOf(req.body.registrationId)==-1){
      user.gcm.registrationIds.push(req.body.registrationId);
      user.save(function(err) {
        if (err) {
          console.log("returning validationError");
          return validationError(res, err);
        }else{
          console.log("returning 200");
          return res.status(200).json(user.gcm);
        }
      });
    }else{
      console.log("returning 200");
      return res.status(200).json(user.gcm);
    }
  });
};

/**
 * Tests GCM sending a message to the user
 */
exports.testGCM = function(req, res, next) {
  console.log("user.controller.js.testGCM")
  User.findById(req.user._id, function (err, user) {
    if(err){
      return res.status(500).json();
    }
    GCM.sendMessage(user.gcm.registrationIds, req.body, true, 5);
    return res.status(418).json();
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  console.log("user.controller.js.me")
  var userId = req.user._id;
  var query = User
  .findOne({_id: userId})
  .select('-salt -hashedPassword')
  .populate('profile.subscriptionType profile.profession profile.questionaries');
  query.exec(function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).json({ subcode:105, message: "user doesn't exist" });
    if (!user.profile.subscriptionType) {
      SubscriptionType.findOne({'name': 'basic'}).exec(function(err, subscriptionType){
        if(err) {
          res.status(500).json(err);
        }
        user.profile.subscriptionType = subscriptionType;
        user.save(function(err, user) {
          if (err) {
            return validationError(res, err);
          }
          User.populate(user, {
            path: 'profile.questionaries.questionaryAnswers',
            select: 'price',
            model: QuestionaryAnswer
          }, function(err, userPopulated){
            if(err) {
              res.status(500).json(err);
            }
            res.status(200).json(userPopulated);
          })
        });
      });
    } else {
      //res.status(200).json(user);
          if (err) {
            return validationError(res, err);
          }
          User.populate(user, {
            path: 'profile.questionaries.questionaryAnswers',
            select: 'price ngo',
            model: QuestionaryAnswer
          }, function(err, userPopulated){
            if(err) {
              res.status(500).json(err);
            }
            res.status(200).json(userPopulated);
          })
    }
  });
};

exports.myDonation = function(req, res, next) {
  console.log("user.controller.js.myDonation")
  var userId = req.user._id;
  console.log(req.user)
  User
  .findOne({_id: userId})
  .select('-salt -hashedPassword')
  .populate('profile.subscriptionType profile.profession profile.questionaries')
  .exec(function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.status(401).json({ subcode:105, message: "user doesn't exist" });
        //res.status(200).json(user);
        console.log("About to populate QuestionaryAnswers")
        if (err) {
          return validationError(res, err);
        }
        User.populate(user, {
          path: 'profile.questionaries.questionaryAnswers',
          select: 'price ngo',
          model: QuestionaryAnswer
        }, function(err, userPopulated){
          console.log(userPopulated);
          if(err) {
            res.status(500).json(err);
          }
          var current = {};
          var dict = {};
          
          //Si no hay questionarios
          if(userPopulated.profile.questionaries.length==0){
            console.log("Si no hay questionarios");
            return res.status(200).json([]);
          }
          //Si hay questionarios pero no tienen respuestas
          /*
          if(!userPopulated.profile.questionaries.length || !userPopulated.profile.questionaries[0].questionaryAnswers[0]){
            console.log("Si hay questionarios pero no tienen respuestas");
            return res.status(200).json(dict);
          }
          */

          //Por cada questionario
          userPopulated.profile.questionaries.forEach(function(questionary){
            //Por cada respuesta en el cuestionario
            questionary.questionaryAnswers.forEach(function(questionaryAnswer){
              
              if(questionaryAnswer && (dict[questionaryAnswer.ngo]===null || dict[questionaryAnswer.ngo]===undefined)){
                dict[questionaryAnswer.ngo] = 0;
              }
              dict[questionaryAnswer.ngo] = dict[questionaryAnswer.ngo]+questionaryAnswer.price;
            });
          });

          console.log("Todo ok");
          return res.status(200).json(dict);
        })
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};

exports.refresh = function (req, res, next) {
  var oldToken = req.body.access_token;
  if (!oldToken) {
    console.log("error 1");
    res.status(401).json({ subcode:104, message: 'Old access_token is required.' });
  }
  var refreshToken = req.body.refresh_token;
  if (!refreshToken) {
    console.log("error 2");
    res.status(401).json({ subcode:103, message: 'refresh_token is required.' });
  }

  var decoded = jwt.decode(oldToken);

  var userId = decoded._id;

  User.findOne({_id: userId, refreshToken: refreshToken}, function (err, user) {
    if (err) return next(err);
    if (!user) return res.status(401).json("User not found");
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: config.expirationTime });
    res.json({
      token: token,
      info: user.info
    });
  });
};

exports.resetPassword = function(req, res){
  jwt.verify(req.body.refreshPasswordToken, config.secrets.session, {ignoreExpiration: false}, function(err, decoded){
    if(err){
      console.log(err)
      return res.status(500).json();
    }if(!decoded){
      return res.status(404).json();
    }
    console.log(decoded)
    if(!decoded.resetPassword){
      return res.status(401).json({ subcode: 102, message:"Invalid reset Token" });
    }
    User
    .findOne({'_id': decoded._id})
    .exec(function(err, user) {
        //console.log(user.authenticate(req.body.password));
        user.password = req.body.newPassword;

        user.save(function(err, doc){
        if(err){
          return res.status(500).json();
        }
        var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: config.expirationTime });
        return res.status(200).json({
          token: token,
          info: user.info
        });
      });
    });
  });
}

exports.requestResetPassword = function(req, res){
  User
  .findOne({'email': req.body.email})
  .exec(function(err, user){
    if(err){
      return res.status(500).json();
    }if(!user){
      return res.status(404).json({ subcode:101, message:"User doesn't exist" });
    }
    var nodemailer = require('nodemailer');
    // create reusable transporter object using SMTP transport
    var transporter = nodemailer.createTransport({
        host: 'smtp.1and1.es',
        port: '587',
        auth: {
            user: 'lacoume@byvapps.com',
            pass: 'lacoumeBYVapps'
        }
    });

    // NB! No need to recreate the transporter object. You can use
    // the same transporter object for all e-mails

    //Generate new token
    var token = jwt.sign({ _id: user._id, resetPassword: true }, config.secrets.session, { expiresInMinutes: 60 });
    console.log(token)

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'Groween <lacoume@byvapps.com>', // sender address
        to: req.body.email, // list of receivers
        subject: 'Groween password reset', // Subject line
        text: 'http://' + req.headers.host + '/reset?email='+req.body.email+'&token='+token // plaintext body //TODO típico mensaje de "Si no ha solicitado esto... bla bla bla"
        //html: '<b>Hello world ✔</b>' // html body //TODO
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return res.status(500).json(error);
        }else{
            console.log('Message sent: ' + info.response);
            return res.status(200).json();
        }
    });
  });
}
