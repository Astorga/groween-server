'use strict';
console.log('area.controller.js');

var _ = require('lodash');
var Area = require('./area.model');
var Template = require('../template/template.model');
var mongoose = require('mongoose')


// Get list of areas
exports.index = function(req, res) {
	console.log("index");
	console.log(req.query)
	var emptyProfession;
	if(req.query.emptyProfession=='false'){ emptyProfession = false;
	} else emptyProfession = true;
	var query = Area
	.find()
	.populate('categories')
	.sort('-created_at');
	if(req.query.updated_at){
		query.where('updated_at').gt(req.query.updated_at);
	}
	query.exec(function(err, areas) {
	    if(!err){
	    	//Profession things
	    	if(req.query.profession){
		    	for (var i = areas.length - 1; i >= 0; i--) {
		    		for (var j = areas[i].categories.length - 1; j >= 0; j--) {
		    			if(!emptyProfession && !areas[i].categories[j].profession){
		    				areas[i].categories.splice(j, 1)
		    			}else{
		    				if(areas[i].categories[j].profession && areas[i].categories[j].profession != req.query.profession){
		    					areas[i].categories.splice(j, 1)
		    				}
		    			}
		    		};
		    	};
	    	}else{
	    		for (var i = areas.length - 1; i >= 0; i--) {
		    		for (var j = areas[i].categories.length - 1; j >= 0; j--) {
		    			if(!emptyProfession && !areas[i].categories[j].profession){
		    				areas[i].categories.splice(j, 1)
		    			}
		    		};
		    	};
	    	}
	    	///Profession things
	    	res.json(areas);
	    } else {
			return res.status(400).json("Error: could not retrieve areas: " + areas);
		}
	});
};

// Get a single area
exports.show = function(req, res) {
	Area
	.findOne( { _id: req.params.idArea } )
	.populate('categories')
	.exec(function(err, area){
		if(!err){
			return res.json(area);
		} else {
			return res.status(400).json("Error: could not retrieve area: " + area);
		}
	});
};

// Creates a new area in the DB.
exports.create = function(req, res) {
	Area.create(req.body, function(err, area){
		if(!err){
			return res.json(area);
		} else {
			return res.status(400).json(err);
		}
	});
};

// Replaces existing area with obtained data.
exports.update = function(req, res) {
    console.log(req.body);
  if(req.body._id) { delete req.body._id; }
  Area.findById(req.params.id, function (err, area) {
    if (err) { 
    	return handleError(res, err);
    }
    if(!area) {
    	return res.status(404).json(); 
    }
    if(!req.body.categories) { area.categories = []; }
    var updated = _.merge(area, req.body);
    updated.save(function (err) {
      if (err) { 
      	return handleError(res).json(err); 
      }
      return res.status(200).json(area);
    });
  });
};

// Patches an existing area in the DB changing some of its data.
exports.patch = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Area.findById(req.params.idArea, function (err, area) {
    if (err) { 
    	return handleError(res, err); 
    }
    if(!area) { 
    	return res.status(404).json(); 
    }
    var updated = _.merge(area, req.body);
    updated.save(function (err) {
      if (err) { 
      	return handleError(res).json(err); 
      }
      return res.status(200).json(area);
    });
  });
};

// Deletes a area from the DB.
exports.destroy = function(req, res) {
  	Area.findById(req.params.idArea, function (err, area) {
    if(err) { return handleError(res, err); }
    if(!area) { return res.status(404).json(); }
    area.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

// Gets categories
exports.getCategories = function(req, res){
	Area
	.findOne({'_id': req.params.idArea})
	.populate('categories categories.topics')
	.exec(function(err, area){
	    if(err){
			return res.status(400).json("Error: could not retrieve categories: " + area.categories);
		}
		if(!area){
			return res.status(404).json();
		}
		return res.status(area.categories);
	});
}

// Gets a category
exports.getCategory = function(req, res){
	Area
	.findOne({'_id': req.params.idArea})
	.populate('categories categories.topics')
	.exec(function(err, area){
	    if(err){
			return res.status(400).json("Error: could not retrieve category: " + req.params.idCategory);
		}
		if(!area){
			return res.status(404).json();
		}
		return res.status(area.categories.id(req.params.idCategory));
	});
}

// Adds a category to the area
exports.addCategory = function(req, res){
	Area
	.findOne({'_id': req.params.idArea})
	.populate('categories categories.topics')
	.exec(function(err, area){
		if(err){
			return res.status(404).json();
		}
		var Category = mongoose.model('Category');
		Category.create(req.body, function(err, category){
			if(err){
				return res.status(400).json(err+'');
			}
			area.categories.push(category);
	    	area.save(function(err){
	    		if(err){
					return res.status(500).json(err+'');
	    		}else{
	    			return res.status(201).json(category);
	    		}
	    	});
		});
	});
}

// Deletes a category from the area
exports.deleteCategory = function(req, res){
	Area
	.findOne({'_id': req.params.idArea})
	.populate('categories categories.topics')
	.exec(function(err, area){
		if(err){
			return res.status(404).json();
		}
		var category = area.categories.id(req.params.idCategory);
		category.remove(function(err){
			if(err){
				return res.status(500).json();
			}
			area.categories.splice(area.categories.indexOf(req.params.idCategory), 1);
	    	area.save(function(err){
	    		if(err){
					return res.status(500).json(err+'');
	    		}else{
	    			return res.status(204).json();
	    		}
	    	});
		});
	});
}

function handleError(res, err) {
	console.log("area.controller.handleError");
  return res.status(500).json(err);
}
