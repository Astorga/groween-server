'use strict';

var _ = require('lodash');
var Ngo = require('./ngo.model');

// Get list of ngos
exports.index = function(req, res) {
  if(!req.query.orderBy){
    Ngo.find()
      .populate('image')
      .exec(function (err, ngos) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(ngos);
    });
  }else{
    Ngo.find()
      .populate('image')
      .sort(req.query.orderBy)
      .exec(function (err, ngos) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(ngos);
    });
  }
};

// Get list of active ngos
exports.actives = function(req, res) {
  if(!req.query.orderBy){
    Ngo.find({active: true})
      .select('name description url totalDonation')
      .populate('image')
      .exec(function (err, ngos) {
        if(err) { return handleError(res, err); }
        return res.status(200).json(ngos);
      });
  }else{
    Ngo.find({active: true})
    .select('name description url totalDonation')
    .populate('image')
    .sort(req.query.orderBy)
    .exec(function (err, ngos) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(ngos);
    });
  }
};

// Get list of active ngos: top 3 by total donation
exports.tops = function(req, res) {
  Ngo.find({active: true})
    .select('name description url totalDonation')
    .populate('image')
    .limit(3)
    .sort({'totalDonation': -1})
    .exec(function (err, ngos) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(ngos);
    });
};

// Get a single ngo
exports.show = function(req, res) {
  Ngo.findOne({ _id: req.params.id })
    .populate('image')
    .exec(function (err, ngo) {
    if(err) { return handleError(res, err); }
    if(!ngo) { return res.status(404).json(); }
    return res.status(200).json(ngo);
  });
};

// Creates a new ngo in the DB.
exports.create = function(req, res) {
  if(req.body.totalDonation) { delete req.body.totalDonation; }
  Ngo.create(req.body, function(err, ngo) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(ngo);
  });
};

// Updates an existing ngo in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  if(req.body.totalDonation) { delete req.body.totalDonation; }
  Ngo.findOne({ _id: req.params.id })
    .populate('image')
    .exec(function (err, ngo) {
    if (err) { return handleError(res, err); }
    if(!ngo) { return res.status(404).json(); }
    _.merge(ngo, req.body);
    ngo.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ngo);
    });
  });
};

// Deletes a ngo from the DB.
exports.destroy = function(req, res) {
  Ngo.findById(req.params.id, function (err, ngo) {
    if(err) { return handleError(res, err); }
    if(!ngo) { return res.status(404).json(); }
    ngo.remove(function(err) {
      if(err) { return res.status(409).json(err); }
      return res.status(204).json();
    });
  });
};

exports.totalDonation = function(req, res) {
  var totalDonation = 0;

  Ngo
  .find()
  .select('totalDonation')
  .exec(function(err, docs){
    if(err){
      return res.status(500).json()
    }
    return res.status(200).json(docs);
  });
}

function handleError(res, err) {
  return res.status(500).json(err);
}
