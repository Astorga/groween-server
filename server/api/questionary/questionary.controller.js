'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var Questionary = require('./questionary.model');
var Question = require('../question/question.model');
var QuestionaryAnswer = require('../questionaryAnswer/questionaryAnswer.model');
var User = require('../user/user.model');

// Get list of questionarys
exports.index = function(req, res) {
  Questionary
  .where('_id').in(req.user.profile.questionaries)
  .sort('updated_at')
  .populate('questions questionaryAnswers template selfAnswer')
  .exec(function(err, questionaries){
    if(err){
      return res.status(500).json(err)
    }
    return res.status(200).json(questionaries)
  });

  //res.status(200).json(req.user.profile.questionaries);
};

// Get list of questionarys
exports.indexAnswers = function(req, res) {
  var ok = false;
  for (var i = req.user.profile.questionaries.length - 1; i >= 0; i--) {
    if(req.user.profile.questionaries[i]._id==req.params.id){
      ok = true;
      var result = req.user.profile.questionaries[i];
      Questionary.populate(result, {path:"questionaryAnswers"}, function(err, populatedQuestionary){
        if(err) console.log(err);
        else return res.status(200).json(populatedQuestionary.questionaryAnswers);
      });
    }
  };

  if(!ok){
    Questionary
    .findOne({'_id': req.params.id})
    .populate('questions questionaryAnswers template selfAnswer')
    .exec(function(err, questionary){
      if(err){
        return res.status(500).json();
      }
      if(!questionary){
        return res.status(404).json();
      }
      return res.status(401).json();
    });
  }
}


exports.indexUpdated = function(req, res){
  var news = []
  var updated = []
  var erased = []

  var allUserIds = [];

  Questionary
  .where('_id').in(req.user.profile.questionaries)
  .sort('updated_at')
  .populate('questions questionaryAnswers template selfAnswer')
  .exec(function(err, questionaries){
    if(err) {
      return res.status(500).json(err)
    }
    for (var i = 0; i<questionaries.length; i++) {
      allUserIds.push(questionaries[i]._id.toString())
    };

    if (!req.query.ids) {
      req.query.ids = [];
    }
    if (typeof req.query.ids == 'string') {
      req.query.ids = [req.query.ids];
    }
    for (var i = 0; i<req.query.ids.length; i++) {
      if(allUserIds.indexOf(req.query.ids[i])==-1){ //Deleted
        erased.push(req.query.ids[i]); //Add to deleted
      }
    };

    for (var i = questionaries.length - 1; i >= 0; i--) {
      if(req.query.ids.indexOf(allUserIds[i])!=-1) {
        if(questionaries[i].updated_at>req.query.time){
          for (var j = questionaries[i].questionaryAnswers.length - 1; j >= 0; j--) {
            if(questionaries[i].questionaryAnswers[j].created_at<req.query.time){ //If old
              questionaries[i].questionaryAnswers.splice(j, 1)  //erase
            }
          }
          updated.push(questionaries[i])  //Add to updated
        }
      }
    };

    Questionary
    .where('_id').in(req.user.profile.questionaries).nin(req.query.ids)
    .sort('updated_at')
    .populate('questions questionaryAnswers template selfAnswer')
    .exec(function(err, docs){
      if(err){
        return res.status(500).json(err)
      }
      news = docs;
      var respuesta = {
        news: news,
        updated: updated,
        erased: erased
      }
      //var respuesta = []
      //respuesta.push(news)
      //respuesta.push(updated)
      //respuesta.push(erased)
      return res.json(respuesta);
    });
  });
}

// Get a single questionary
exports.show = function(req, res) {
  var ok = false;
  for (var i = req.user.profile.questionaries.length - 1; i >= 0; i--) {
    if(req.user.profile.questionaries[i]._id==req.params.id){
      ok = true;
      var result = req.user.profile.questionaries[i];
      Questionary.populate(result, {path:"questions questionaryAnswers template selfAnswer"}, function(err, populatedQuestionary){
        console.log(err);
        return res.status(200).json(populatedQuestionary);
      });
    }
  };

  if(!ok){
    Questionary
    .findOne({'_id': req.params.id})
    .populate('questions questionaryAnswers template selfAnswer')
    .exec(function(err, questionary){
      if(err){
        return res.status(500).json();
      }
      if(!questionary){
        return res.status(404).json();
      }
      return res.status(401).json();
    });
  }
}

// Get a single public questionary
exports.public = function(req, res) {
  User.findOne({ 'profile.questionaries': req.params.id})
    .populate('profile.questionaries')
    .exec(function(err, user) {
      if(err){
        return res.status(500).json();
      }
      if(user){
        console.log(user);
        for (var i = 0; i < user.profile.questionaries.length; i++) {
          console.log(i);
          if ( user.profile.questionaries[i]._id == req.params.id) {
            var result = user.profile.questionaries[i].toObject();
            Questionary.populate(result, {path:"questions"}, function(err, populatedQuestionary){
              console.log(err);
              //result = result.toObject();
              result.userName = user.profile.name;
              delete result.created_at;
              delete result.updated_at;
              delete result.__v;
              delete result.questionaryAnswers;

              return res.status(200).json(result);
            });
            //return res.status(200).json(result);
          }
        }
      }
      else {
        return res.status(404).json();
      }

    });
  /*
  Questionary
    .findOne({'_id': req.params.id})
    .select('id name questions active')
    .populate('questions')
    .exec(function(err, questionary){
      if(err){
        return res.status(500).json();
      }
      if(!questionary){
        return res.status(404).json();
      } else {
        return res.status(200).json(questionary);
      }
    });
    */
};


function test(question){
  Question
  .create(question, function(err, question){
    if(err){
      console.log("ERROR   on creating subQuestion")
      console.log("returning...")
      return "ERROR"
    }
    console.log("SUCCESS on creating subQuestion")
    console.log(question)
    console.log("returning...")
    return question;
  });
}

function recursive(res, position, req, ids){
  var questionJson;
  console.log("recursive " + position)

  questionJson = req.body.questions[position];
  if (typeof req.body.questions == 'string') {
    req.body.questions = JSON.parse(req.body.questions);
    questionJson = req.body.questions[position];
  }
  if (typeof req.body.questions[position] == 'string') {
    questionJson = JSON.parse(req.body.questions[position]);
  }

  if(questionJson){
    if(questionJson.topic && questionJson.topic._id){
      questionJson.topic = questionJson.topic._id;
    }

    Question
    .create(questionJson, function(err, question){
      if(err){
        console.log("ERROR   on creating subQuestion")
        console.log("returning...")
        return res.status(500).json(err);
      }
      ids.push(question._id);
      position = position+1;
      recursive(res, position, req, ids);
    });
  }else{
    req.body.questions = ids;
    Questionary.create(req.body, function(err, questionary) {
    if(err) {
      return handleError(res, err);
    }else{
      req.user.profile.questionaries.push(questionary);
      req.user.save(function(err, user){
        if(err){
          return res.status(500).json(err);
        }else{
          Questionary.populate(questionary, {path:"questions questionaryAnswers template selfAnswer"}, function(err, populatedQuestionary){
            return res.status(201).json(populatedQuestionary);
          });
          //return res.status(201).json(questionary);
        }
      });
    }
  });
  }
}

// Creates a new questionary in the DB.
exports.create = function(req, res) {
  var ids = [];

  Questionary
  .find({'active': true})
  .where('_id').in(req.user.profile.questionaries)
  .exec(function(err, questionaries){
    if(err){
      return res.status(500).json(err)
    }
    if(questionaries.length>=req.user.profile.subscriptionType.numberOfActives || !req.user.profile.subscriptionType.numberOfActives){
      res.status(403).json();
    }else{
      recursive(res, 0, req, ids);
    }
  });
};

// Creates or replaces selfAnswer on questionary in the DB.
exports.createSelfAnswer = function(req, res) {
  Questionary
  .findOne({'_id': req.params.id})
  //.populate('questions questionaryAnswers template selfAnswer')
  .exec(function(err, questionary){
    if(err){
      return res.status(500).json();
    }
    if(!questionary){
      return res.status(404).json();
    }
    //*Magic happens*
    //Format things
    if (typeof req.body.answers == 'string') {
      req.body.answers = req.body.answers.split(" ").join("");
      req.body.answers = req.body.answers.split("]").join("");
      req.body.answers = req.body.answers.split("[").join("");
      req.body.answers = req.body.answers.split(",");
    }
    //Format things
    if(questionary.selfAnswer){
      console.log("TODO Delete previous selfAnswer")
      QuestionaryAnswer
      .findOne({'_id': questionary.selfAnswer})
      .remove(function (error, data){ // NO LANZA LOS HOOKS
        console.log(error + " | " + data);
        console.log("Creating new one...")
        QuestionaryAnswer.create(req.body, function(err, questionaryAnswer) {
        if(err) { console.log(err); return res.status(500).json(err); }
          questionary.selfAnswer = questionaryAnswer;
          questionary.save(function(err){
            if(err){
              console.log(err);
              return res.status(500).json(err);
            }
            Questionary.populate(questionary, {path:"questions questionaryAnswers template selfAnswer"}, function(err, populatedQuestionary){
              return res.status(201).json(populatedQuestionary);
            });
          });
        });
      });
    }else{
      console.log("There is no previous selfAnswer to delete")
      console.log("Creating new one...")
      QuestionaryAnswer.create(req.body, function(err, questionaryAnswer) {
        if(err) { console.log(err); return res.status(500).json(err); }
        questionary.selfAnswer = questionaryAnswer;
        questionary.save(function(err){
          if(err){
            console.log(err);
            return res.status(500).json(err);
          }
          Questionary.populate(questionary, {path:"questions questionaryAnswers template selfAnswer"}, function(err, populatedQuestionary){
            return res.status(201).json(populatedQuestionary);
          });
        });
      });
    }
    ///Delete
    //Create new selfAnswer on DB
    //Assign it to questionary
  });
};

// Updates an existing questionary in the DB.
exports.update = function(req, res) {
  console.log(req.body)
  if(eval(req.body.active)){
    var actives = 0;
    for (var i = req.user.profile.questionaries.length - 1; i >= 0; i--) {
      if(req.user.profile.questionaries[i].active){
        actives += 1;
      }
    };
    if(actives >= req.user.profile.subscriptionType.numberOfActives){
      return res.status(403).json();
    }
  }
  if(req.body._id) { delete req.body._id; }
  Questionary.findById(req.params.id, function (err, questionary) {
    if (err) { return handleError(res, err); }
    if(!questionary) { return res.status(404); }
    var updated = _.merge(questionary, req.body);
    updated.save(function (err, last) {
      if (err) { return handleError(res, err); }
      Questionary.populate(last, {path:"template questions selfAnswer questionaryAnswers"}, function(err, last) {
        return res.status(200).json(last);
      });
    });
  });
};

// Deletes a questionary from the DB.
exports.destroy = function(req, res) {
  Questionary.findById(req.params.id, function (err, questionary) {
    if(err) { return handleError(res, err); }
    if(!questionary) { return res.status(404); }
    questionary.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

exports.mirror = function(req, res) {
  if(req.query.code){
    res.status(req.query.code)
  }
  if(req.query.json){
    console.log("json")
    return res.json(req.query.json)
  }else{
    console.log("noJson")
    return res.json()
  }
};

function handleError(res, err) {
  return res.status(500).json(err);
}
