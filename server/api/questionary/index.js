'use strict';

var express = require('express');
var controller = require('./questionary.controller');
var answerController = require('../questionaryAnswer/questionaryAnswer.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

//Questionary
router.get('/', auth.isAuthenticated(), controller.index);
router.get('/updated', auth.isAuthenticated(), controller.indexUpdated);
router.get('/mirror', controller.mirror); //TODO delete
router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/:id/public', controller.public);
router.post('/:id/selfAnswer', auth.isAuthenticated(), controller.createSelfAnswer);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

//QuestionaryAnswers
router.get('/:id/questionaryAnswers', auth.isAuthenticated(), controller.indexAnswers);

module.exports = router;
