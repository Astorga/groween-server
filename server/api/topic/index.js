'use strict';

var express = require('express');
var questionController = require('../question/question.controller');
var reinforceContentController = require('../reinforceContent/reinforceContent.controller');

var router = express.Router();

//Topics

//Questions
router.get('/:idTopic/questions', questionController.index);
//router.post('/:idTopic/topics', controller.addTopic);

//ReinforceContents
router.get('/:idTopic/ReinforceContents', reinforceContentController.index);

module.exports = router;