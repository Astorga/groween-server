'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var QuestionaryAnswerSchema = new Schema({
  answers: 				[ { type: Number, default: 1 } ]
  , comment: 			{ type: String, trim: true }
  , price: 			 	{ type: Number, default: 0 }
  , ngo: 				{ type: Schema.ObjectId, ref: 'NGO' }
  , created_at: 		{ type: Number }
  , updated_at: 		{ type: Number }
});

QuestionaryAnswerSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

QuestionaryAnswerSchema.post('save', function (doc) {
	if(doc.ngo){
	  var NGO = mongoose.model('NGO');
	  NGO
	  .findOne({'_id': doc.ngo})
	  .exec(function(err, ngo){
	  	if(err) {
	  		console.log('ERROR on questionaryAnswer.model.js.save.post: ' + err);
	  	}else if(!ngo) {
	  		console.log('ERROR on questionaryAnswer.model.js.save.post: NGO not found');
	  	}else{
	  		ngo.totalDonation = ngo.totalDonation + doc.price;
	  		ngo.save(function(err){
	  			if(err){
	  				console.log('ERROR on questionaryAnswer.model.js.save.post: ' + err);
	  			}else{
	  				//Do nothing
	  			}
	  		});
	  	}
	  });
	}
});

module.exports = mongoose.model('QuestionaryAnswer', QuestionaryAnswerSchema);
