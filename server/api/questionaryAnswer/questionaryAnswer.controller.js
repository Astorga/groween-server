'use strict';

var _ = require('lodash');
var QuestionaryAnswer = require('./questionaryAnswer.model');
var Questionary = require('../questionary/questionary.model');
var User = require('../user/user.model');
var GCM = require('../../gcm/gcm.controller');

// Get list of questionaryAnswers
exports.index = function(req, res) {
  if(req.params.idQuestionary){ //Lista de respuestas a un cuestionario
    QuestionaryAnswer
    .find({'questionary': req.params.idQuestionary})
    .exec(function (err, questionaryAnswers) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(questionaryAnswers);
    });
  }else{ //Lista de respuestas a un usuario
    var ids = []
    for (var i = req.user.profile.questionaries.length - 1; i >= 0; i--) {
      ids.push(req.user.profile.questionaries[i]._id)
    };
    QuestionaryAnswer
    .where('questionary').in(ids)
    .exec(function (err, questionaryAnswers) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(questionaryAnswers);
    });
  }
};

exports.indexNew = function(req, res){
  var ids = []
  for (var i = req.user.profile.questionaries.length - 1; i >= 0; i--) {
    ids.push(req.user.profile.questionaries[i]._id)
  };
  QuestionaryAnswer
  .where('questionary').in(ids)
  .where('created_at').gt(req.query.time)
  .exec(function (err, questionaryAnswers) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(questionaryAnswers);
  });
}

// Get a single questionaryAnswer
exports.show = function(req, res) {
  QuestionaryAnswer.findById(req.params.id, function (err, questionaryAnswer) {
    if(err) { return handleError(res, err); }
    if(!questionaryAnswer) { return res.status(404).json(); }
    return res.json(questionaryAnswer);
  });
};

// Creates a new questionaryAnswer in the DB. if it's selfAnswer send selfAnswer: true
exports.create = function(req, res) {
    console.log("questionaryAnswer.controller.js.create")
  req.body.price = 0;
  if(req.body.ngo){
    //TODO: get price from Database
    req.body.price = 0.01;
  }

  Questionary
  .findOne({'_id': req.body.questionary})
  .exec(function(err, questionary){
    
    if(questionary.pushOnNewAnswer){
      User
      .findOne({ 'profile.questionaries': questionary._id})
      .exec(function(err, user){
        if(err){
          //Do nothing
        }else{
          var notification = {};
          notification.sound = "default";
          notification.title_loc_key = "NOT_TITLE_NEW_ANSWER";
          notification.loc_key = "NOT_CONTENT_NEW_ANSWER";
          notification.loc_args = [questionary.name];

          var data = {};
          data.type = "new_questionaryanswer"
          data.questionary = {};
          data.questionary._id = questionary._id;
          data.questionary.name = questionary.name;
          GCM.sendMessage(user.gcm.registrationIds, notification, data, true, 5);
        }
      });
    }

    if(err){
      return res.status(500).json(err)
    }else if(!questionary){
      return res.status(404).json();
    }
    QuestionaryAnswer.create(req.body, function(err, questionaryAnswer) {
      if(err) { return handleError(res, err); }
      questionary.questionaryAnswers.push(questionaryAnswer);
      questionary.save(function(err){
        if(err){
          return res.status(500).json(err);
        }
        return res.status(201).json(questionaryAnswer);
      });
    });
  });
};

// Updates an existing questionaryAnswer in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  QuestionaryAnswer.findById(req.params.id, function (err, questionaryAnswer) {
    if (err) { return handleError(res, err); }
    if(!questionaryAnswer) { return res.status(404).json(); }
    var updated = _.merge(questionaryAnswer, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(questionaryAnswer);
    });
  });
};

// Deletes a questionaryAnswer from the DB.
exports.destroy = function(req, res) {
  QuestionaryAnswer.findById(req.params.id, function (err, questionaryAnswer) {
    if(err) { return handleError(res, err); }
    if(!questionaryAnswer) { return res.status(404).json(); }
    questionaryAnswer.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}
