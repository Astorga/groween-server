'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var Area = mongoose.model('Area');
var Profession = require('../profession/profession.model');

// Get a single category
exports.show = function(req, res) {
  Area
  .findOne({'categories._id': req.params.idCategory})
  .exec(function(err, area){
  	if(err){
  		return res.status(500).json(err);
  	}else if(!area){
  		return res.status(404).json();
  	}else{
  		return res.status(200).json(area.categories.id(req.params.idCategory));
  	}
  });
};

// Updates an existing category in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Area
  .findOne({'categories._id': req.params.idCategory})
  .exec(function(err, area){
  	if(err){
  		return res.status(500).json(err);
  	}else if(!area){
  		return res.status(404).json();
  	}else{
  		var updated = _.merge(area.categories.id(req.params.idCategory), req.body);
	    area.save(function (err) {
	      if (err) { return handleError(res, err); }
	      return res.status(200).json(updated);
	    });
  	}
  });
  /*Category.findById(req.params.idCategory, function (err, category) {
    if (err) { return handleError(res, err); }
    if(!category) { return res.status(404).json(); }
    var updated = _.merge(category, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(category);
    });
  });*/
};

// Deletes a category from the DB.
exports.destroy = function(req, res) {
  var Area = mongoose.model('Area');
  Area
  .findOne({'categories._id': req.params.idCategory})
  .exec(function (err, area) {
    if(err){
  		return res.status(500).json(err);
  	}else if(!area){
  		return res.status(404).json();
  	}else{
  		var category = area.categories.id(req.params.idCategory);
  		category.remove(function(err){
  			if(err){
  				res.status(500).json(err)
  			}else{
				area.categories.splice(area.categories.indexOf(category), 1);
	  			area.save(function(err){
		  			if(err){
		  				return res.status(500).json(err);
		  			}else{
		  				category.remove(function(err){
				  			if(err){
				  				return res.status(500).json(err);
				  			}else{
				  				return res.status(204).json();
				  			}
				  		});
		  			}
		  		});
  			}
  		});
  	}
  });
};

// Gets topics
exports.getTopics = function(req, res){
	Area
	.findOne({'categories._id': req.params.idCategory})
	.exec(function(err, area){
		if(err){
			return res.status(500).json(err);
		}
		if(!area){
			return res.status(404).json();
		}

		return res.json((area.categories.id(req.params.idCategory)).topics);
	});
}

// Gets a topic
exports.getTopic = function(req, res){
	Area
	.findOne({'categories._id': req.params.idCategory})
	.exec(function(err, area){
		if(err){
			return res.status(500).json(err);
		}if(!area){
			return res.status(404).json();
		}

		return res.status(200).json((area.categories.id(req.params.idCategory)).topics.id(req.params.idTopic));
	});
}

// Adds a topic to the category
exports.addTopic = function(req, res){
	Area
	.findOne({'categories._id': req.params.idCategory})
	.exec(function(err, area){
		if(err){
			return res.status(500).json(err);
		}else{
			if(!area){
				res.status(404).json()
			}
			var category = area.categories.id(req.params.idCategory);
			for (var i = category.topics.length - 1; i >= 0; i--) {
				if(category.topics[i].name==req.body.name){
					return res.status(200).json(category.topics[i]);
				}
			};
			var Topic = mongoose.model('Topic');
			Topic.create(req.body, function(err, topic){
				if(err){
          console.log(err);
					return res.status(500).json(err+'');
				}
				category.topics.push(topic);
		    	area.save(function(err){
		    		if(err){
						return res.status(500).json(err+'');
		    		}else{
		    			return res.status(201).json(topic);
		    		}
		    	});
			});
		}
	});
}

// Deletes a topic from the category
exports.deleteTopic = function(req, res){

	Area
	.findOne({'categories._id': req.params.idCategory})
	.exec(function(err, area){
		if(err){
			return res.status(400).json(err);
		}else{
			var category = area.categories.id(req.params.idCategory)
			var topic = category.topics.id(req.params.idTopic);
			console.log(topic)
			if(topic){
				topic.remove(function(err){
					if(err){
						return res.status(500).json();
					}
					category.topics.splice(category.topics.indexOf(req.params.idTopic), 1);
			    	area.save(function(err){
			    		if(err){
							return res.status(500).json(err+'');
			    		}else{
			    			return res.status(204).json();
			    		}
			    	});
				});
			}else{
				res.status(404).json();
			}
		}
	});
}
