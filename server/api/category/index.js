'use strict';

var express = require('express');
var controller = require('./category.controller');
var templateController = require('../template/template.controller');
var reinforceContentController = require('../reinforceContent/reinforceContent.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

//Categories
router.get('/:idCategory', auth.isAuthenticated(), controller.show);
router.put('/:idCategory', auth.isAuthenticated(), controller.update);
router.delete('/:idCategory', auth.isAuthenticated(), controller.destroy);

//Templates
router.get('/:idCategory/templates', auth.isAuthenticated(), templateController.index);
router.post('/:idCategory/templates', auth.isAuthenticated(), templateController.create);

//Topics
router.get('/:idCategory/topics', auth.isAuthenticated(), controller.getTopics);
router.get('/:idCategory/topics/:idTopic', auth.isAuthenticated(), controller.getTopic);
router.post('/:idCategory/topics', auth.isAuthenticated(), controller.addTopic);
router.delete('/:idCategory/topics/:idTopic', auth.isAuthenticated(), controller.deleteTopic);

//ReinforceContents
router.get('/:idCategory/ReinforceContents', auth.isAuthenticated(), reinforceContentController.index);
router.get('/:idCategory/ReinforceContents/tops', auth.isAuthenticated(), reinforceContentController.tops);

module.exports = router;
