'use strict';

var _ = require('lodash');
var ReinforceContent = require('./reinforceContent.model');
var mongoose = require('mongoose');

var pageSize = 5;

exports.tops = function(req, res){
  mongoose.model('ResourceType')
  .find({'category': req.params.idCategory})
  .select('name tops')
  .exec(function(err, docs){
    if(err){
      return res.status(500).json(err);
    }else if(!docs){
      return res.status(404).json();
    }else{
      return res.json(docs);
    }
  });
}

// Get list of reinforceContents
exports.index = function(req, res) {
  if(!req.query.page){
    req.query.page=0;
  }
  if(req.params.idCategory){
    mongoose.model('Area')
    .findOne({'categories._id': req.params.idCategory})
    .exec(function(err, area){
      if(err){
        return res.status(500).json(err);
      }
      if(!area){
        return res.status(404).json();
      }
      var ids = []
      for (var i = area.categories.id(req.params.idCategory).topics.length - 1; i >= 0; i--) {
        ids.push(area.categories.id(req.params.idCategory).topics[i])
      };
      ReinforceContent
      .where('topics').in(ids)
      .sort('resourceType')
      .skip(pageSize*req.query.page)
      .limit(pageSize)
      .populate('resourceType topics image')
      .exec(function (err, reinforceContents) {
        if(err) {
          return res.status(500).json() 
        }
        if(!reinforceContents){
          return res.status(404).json()
        }
        return res.status(200).json(reinforceContents);
      });
    });
  }else if(req.params.idTopic){
    ReinforceContent
    .find({'topics': req.params.idTopic})
    .sort('resourceType')
    .skip(pageSize*req.query.page)
    .limit(pageSize)
    .populate('resourceType topics image')
    .exec(function (err, reinforceContents) {
      if(err) {
        return res.status(500).json(err) 
      }
      if(!reinforceContents){
        return res.status(404).json()
      }
      return res.status(200).json(reinforceContents);
    });
  }else if(req.query.topics){
    ReinforceContent
    .where('topics').in(JSON.parse(req.query.topics))
    .sort('resourceType')
    .skip(pageSize*req.query.page)
    .limit(pageSize)
    .populate('resourceType topics image')
    .exec(function (err, reinforceContents) {
      if(err) {
        return res.status(500).json(err) 
      }
      if(!reinforceContents){
        return res.status(404).json()
      }
      return res.status(200).json(reinforceContents);
    });
  }else{
    ReinforceContent
    .find()
    .populate('resourceType topics image')
    .sort('resourceType')
    .skip(pageSize*req.query.page)
    .limit(pageSize)
    .exec(function (err, reinforceContents) {
      if(err) { 
        return handleError(res, err); 
      }
      return res.status(200).json(reinforceContents);
    });
  }
};

// Get a single reinforceContent
exports.show = function(req, res) {
  ReinforceContent
  .findOne({ _id: req.params.id })
  .populate('topics')
  .exec(function (err, reinforceContent) {
    if(err) { return handleError(res, err); }
    if(!reinforceContent) { return res.status(404).json(); }
    return res.json(reinforceContent);
  });
};

// Creates a new reinforceContent in the DB.
exports.create = function(req, res) {
  ReinforceContent.create(req.body, function(err, reinforceContent) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(reinforceContent);
  });
};

// Updates an existing reinforceContent in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ReinforceContent.findById(req.params.id, function (err, reinforceContent) {
    if (err) { return handleError(res, err); }
    if(!reinforceContent) { return res.status(404).json(); }
    var updated = _.merge(reinforceContent, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(reinforceContent);
    });
  });
};

// Deletes a reinforceContent from the DB.
exports.destroy = function(req, res) {
  ReinforceContent.findById(req.params.id, function (err, reinforceContent) {
    if(err) { return handleError(res, err); }
    if(!reinforceContent) { return res.status(404).json(); }
    reinforceContent.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}