'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ReinforceContentSchema = new Schema({
  name: 			{ type: String, trim: true, required: true }
  , title: 			{ type: String, trim: true, required: true }
  , author: 		{ type: String, trim: true, required: true }
  , price: 			{ type: Number, trim: true, required: true }
  , image: 			{ type: Schema.ObjectId, ref: 'Image' }
  , created_at:     { type: Number }
  , updated_at:     { type: Number }
  , resourceType: 	{ type: Schema.ObjectId, ref: 'ResourceType', required: true }
  , topics: 		[ { type: Schema.ObjectId, ref: 'Topic' } ]
});

ReinforceContentSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

ReinforceContentSchema.statics.addTopicToReinforceContent = function(idReinforceContent, idTopic, done){
	var ReinforceContent = this;
	var Topic = mongoose.model('Topic');
	ReinforceContent.findOne({ '_id': idReinforceContent }, function(errReinforceContent, reinforceContent) {
		if(!errReinforceContent) {
			Topic.findOne({ '_id': idTopic }, function(errTopic, topic) {
				if(!errTopic) {
					if(reinforceContent.topics.indexOf(idTopic)!=-1) {
						//error?
					} else {
						reinforceContent.topics.push(topic);
					}
					reinforceContent.save(function (err) {
				  		if (!err) {
				  			done(false, reinforceContent);
				  		} else {
				  			done(true, err);
				  		}
					});
				} else {
					done(true, errTopic);
				}
			});
		} else {
			done(true, errReinforceContent);
		}
	});
}

ReinforceContentSchema.statics.deleteTopicFromReinforceContent = function(idReinforceContent, idTopic, done){
	var ReinforceContent = this;
	var Topic = mongoose.model('Topic');
	ReinforceContent.findOne({ '_id': idReinforceContent }, function(errReinforceContent, reinforceContent) {
		if(!errReinforceContent) {
			Topic.findOne({ '_id': idTopic }, function(errTopic, topic) {
				if(!errTopic) {
					if(reinforceContent.topics.indexOf(idTopic)!=-1) {
						reinforceContent.topics.splice(reinforceContent.topics.indexOf(idTopic), 1);
					} else {
						//error?
					}
					reinforceContent.save(function (err) {
				  		if (!err) {
				  			done(false, reinforceContent);
				  		} else {
				  			done(true, err);
				  		}
					});
				} else {
					done(true, errTopic);
				}
			});
		} else {
			done(true, errReinforceContent);
		}
	});
}

module.exports = mongoose.model('ReinforceContent', ReinforceContentSchema);