'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mkdirp = require('mkdirp'),
    rimraf = require('rimraf');

var ImageInstanceSchema = new Schema({
  name:         { type: String, trim: true }
  , width:      { type: Number, required: true }
  , height:     { type: Number, required: true }
  , location:   { type: String }
  , extension:  { type: String }
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
})

var ImageSchema = new Schema({
  location:       { type: String }
  , url: 	        { type: String }
  , images:       [ ImageInstanceSchema ]
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
  , saveOriginal: { type: Boolean }
  , extension:    { type: String }
});

ImageSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

ImageInstanceSchema.pre('save', function (next) {
  this.location = this.location+this.width+"x"+this.height+"/";
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

ImageInstanceSchema.post('remove', function (doc){
  console.log("ImageInstanceSchema.post.remove")
  //Elimina la carpeta root de la imagen y con ella el resto
  rimraf(doc.location, function(err){
    if(err){
      console.log("ERROR   rimraf'fing " + err);
    }else{
      console.log("SUCCESS rimraf'fed ")
    }
  });
});

ImageSchema.post('remove', function (doc) {
  console.log("ImageSchema.post.remove")
  //Elimina la carpeta root de la imagen y con ella el resto
  rimraf(doc.location, function(err){
    if(err){
      console.log("ERROR   rimraf'fing " + err);
    }else{
      console.log("SUCCESS rimraf'fed ")
    }
  });
  /*var fs = require('fs');
  var filePath = doc.location ;
  fs.unlink(filePath, function(err){
    if(err){
      console.log("ERROR   while erasing image file from " + doc.location)
    }else{
      console.log("SUCCESS while erasing image file from " + doc.location)
    }
  });*/
});

module.exports = mongoose.model('Image', ImageSchema);
