'use strict';

var _ = require('lodash');
var Template = require('./template.model');

// Get list of templates (all on a category or all)
exports.index = function(req, res) {
  if(req.user.role=="admin"){
    if(req.params.idCategory){
        var query = Template
        .find({'category': req.params.idCategory})
        .populate('questions')
        .sort('category');
        if(req.query.updated_at){
          query.where('updated_at').gt(req.query.updated_at)
        }
        query.exec(function (err, templates) {
          if(err) {
            return res.status(500).json(err);
          }
          return res.status(200).json(templates);
        });
    }else{
      var query = Template
      .find({'active': true})
      .populate('questions')
      .sort('category');
        if(req.query.updated_at){
          query.where('updated_at').gt(req.query.updated_at)
        }
      query.exec(function (err, templates) {
        if(err) {
          return res.status(500).json(err);
        }
        return res.status(200).json(templates);
      });
    }
  }else{
    if(req.params.idCategory){
        var query = Template
        .find({'category': req.params.idCategory})
        .populate('questions')
        .sort('category');
        if(req.query.updated_at){
          query.where('updated_at').gt(req.query.updated_at)
        }
        query.exec(function (err, templates) {
          if(err) {
            return res.status(500).json(err);
          }
          return res.status(200).json(templates);
        });
    }else{
      var query = Template
      .find({'active': true})
      .populate('questions')
      .sort('category');
        if(req.query.updated_at){
          query.where('updated_at').gt(req.query.updated_at)
        }
      query.exec(function (err, templates) {
        if(err) {
          return res.status(500).json(err);
        }
        return res.status(200).json(templates);
      });
    }
  }
};

// Get list of templates (all on a category or all)
exports.indexNoCategory = function(req, res) {
  if(req.user.role.type=="admin"){
    Template
    .find({'category': undefined})
    .populate('questions')
    .sort(category)
    .exec(function (err, templates) {
      if(err) {
        return res.status(500).json(err);
      }
      return res.status(200).json(templates);
    });
  }else{
    Template
    .find({'category': undefined}, {'active': true})
    .populate('questions')
    .sort('category')
    .exec(function (err, templates) {
      if(err) {
        return res.status(500).json(err);
      }
      return res.status(200).json(templates);
    });
  }
};

// Get a single template
exports.show = function(req, res) {
  Template
  .findOne({ _id: req.params.id })
  .populate('questions')
  .exec(function (err, template) {
    if(err) { return handleError(res).json(err); }
    if(!template) { return res.status(404).json(); }
    return res.json(template);
  });
};

// Creates a new template in the DB.
exports.create = function(req, res) {
  if(!req.body.category && req.params.idCategory){
    req.body.category = req.params.idCategory;
  }
  Template.create(req.body, function(err, area){
    if(!err){
      return res.status(201).json(area);
    } else {
      return res.status(400).json(err);
    }
  });
};

// Updates an existing template in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Template.findById(req.params.id, function (err, template) {
    if (err) { return handleError(res, err); }
    if(!template) { return res.status(404).json(); }
    var updated = _.merge(template, req.body);
    if(req.body.questions){
      updated.questions = req.body.questions;
      updated.markModified('questions');
    }
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(template);
    });
  });
};

// Deletes a template from the DB.
exports.destroy = function(req, res) {
  Template.findById(req.params.id, function (err, template) {
    if(err) { return handleError(res, err); }
    if(!template) { return res.status(404).json(); }
    template.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}
