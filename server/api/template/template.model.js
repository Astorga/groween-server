'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TemplateSchema = new Schema({
  name: 		{ type: String, trim: true, required: true }
  , description: 		{ type: String, trim: true, required: false }
  , active: 	{ type: Boolean, default: false }
  , category: 	{ type: Schema.ObjectId, ref: 'Category' }
  , questions: 	[ { type: Schema.ObjectId, ref: 'Question', required: true } ]
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
});

TemplateSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

TemplateSchema.pre('remove', function (next) {
  console.log("TemplateSchema.remove.pre")
  var questions = this.questions;
  if(!questions || !questions.length){
    console.log("WARNING on TemplateSchema.remove.pre: questions not found, maybe it didn't have any");
    next();
  }else{
    mongoose.model('Question')
    .where('_id').in(questions)
    .remove(function(err){
    	if(err){
    		console.log("ERROR   on TemplateSchema.remove.pre: error finding questions");
    		return next(err);
    	}else{
    		console.log("SUCCESS on TemplateSchema.remove.pre: questions found ad removed");
    		return next();
    	}
    });
  }
});

module.exports = mongoose.model('Template', TemplateSchema);
