'use strict';

var express = require('express');
var controller = require('./template.controller');
var questionController = require('../question/question.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/noCategory', auth.isAuthenticated(), controller.indexNoCategory);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

router.post('/:idTemplate/questions', questionController.create);

module.exports = router;