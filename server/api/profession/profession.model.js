'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProfessionSchema = new Schema({
  name: { type: String, trim: true, required: true, unique: true }
  , created_at:   { type: Number }
  , updated_at:   { type: Number }
});

ProfessionSchema.pre('save', function (next) {
  var now = new Date();
  this.updated_at = now.getTime();
  if (!this.created_at) {
    this.created_at = now.getTime();
  }
  next();
});

module.exports = mongoose.model('Profession', ProfessionSchema);