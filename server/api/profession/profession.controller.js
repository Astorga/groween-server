'use strict';

var _ = require('lodash');
var Profession = require('./profession.model');

// Get list of professions
exports.index = function(req, res) {
  Profession.find(function (err, professions) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(professions);
  });
};

// Get a single profession
exports.show = function(req, res) {
  Profession.findById(req.params.id, function (err, profession) {
    if(err) { return handleError(res, err); }
    if(!profession) { return res.status(404).json(); }
    return res.json(profession);
  });
};

// Creates a new profession in the DB.
exports.create = function(req, res) {
  Profession.create(req.body, function(err, profession) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(profession);
  });
};

// Updates an existing profession in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Profession.findById(req.params.id, function (err, profession) {
    if (err) { return handleError(res, err); }
    if(!profession) { return res.status(404).json(); }
    var updated = _.merge(profession, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(profession);
    });
  });
};

// Deletes a profession from the DB.
exports.destroy = function(req, res) {
  Profession.findById(req.params.id, function (err, profession) {
    if(err) { return handleError(res, err); }
    if(!profession) { return res.status(404).json(); }
    profession.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).json();
    });
  });
};

function handleError(res, err) {
  return res.status(500).json(err);
}