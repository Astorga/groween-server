var config = require('../config/environment');

var gcm = require('node-gcm');

console.log("gcm.controller.js");
console.log("Using sender_id: " + config.gcm_auth_key);

var sender = new gcm.Sender(config.gcm_auth_key);

/*exports.sendMessage = function(regIds, jsonObject, retry, times) {
	console.log("gcm.controller.js.sendMessage")
	console.log(regIds)
	console.log(jsonObject)
	var message = new gcm.Message();
	message.addData('jsonObject', jsonObject);
	console.log(message)
	if(retry){
		if(times){
			sender.send(message, regIds, times, function (err, result) {
			    if(err) console.error("ERROR:   " + JSON.stringify(err));
			    else    console.error("SUCCESS: " + JSON.stringify(result));
			});
		}else{
			sender.send(message, regIds, times, function (err, result) {
			    if(err) console.error("ERROR:   "  + JSON.stringify(err));
			    else    console.error("SUCCESS: " + JSON.stringify(result));
			});
		}
	}else{
		sender.sendNoRetry(message, regIds, function (err, result) {
		    if(err) console.error("ERROR:   " + JSON.stringify(err));
		    else    console.error("SUCCESS: " + JSON.stringify(result));
		});
	}
};*/

exports.sendMessage = function(regIds, notification, data, retry, times) {
    console.log("gcm.controller.js.sendMessage")
    console.log("regIds: " + JSON.stringify(regIds))
    console.log("notification: " + JSON.stringify(notification))
    console.log("data: " + JSON.stringify(data))
    console.log("retry: " + JSON.stringify(retry))
    console.log("times: " + JSON.stringify(times))
    var message = new gcm.Message();

    if (notification) {
        if (!isJson(notification)) {
            console.log("preParse")
            notification = JSON.parse(notification)
            console.log("postParse")
        }
        console.log("notification")
        for (var key in notification) {
            if (notification.hasOwnProperty(key)) {
                console.log(key + " -> " + notification[key]);
                message.addNotification(key, notification[key]);
            }
        }
    }

    if (data) {
        if (!isJson(data)) {
            console.log("preParse")
            data = JSON.parse(data)
            console.log("postParse")
        }
        console.log("data")
        for (var key in data) {
        	console.log(key)
            if (data.hasOwnProperty(key)) {
                console.log(key + " -> " + data[key]);
                message.addData(key, data[key]);
            }
        }
    }

    if (data || notification) {
        if (retry) {
            if (times) {
                sender.send(message, regIds, times, function(err, response) {
                    if (err) console.error("ERROR:   " + JSON.stringify(err));
                    else console.error("SUCCESS: " + JSON.stringify(response));
                });
            } else {
                sender.send(message, regIds, times, function(err, response) {
                    if (err) console.error("ERROR:   " + JSON.stringify(err));
                    else console.error("SUCCESS: " + JSON.stringify(response));
                });
            }
        } else {
            sender.sendNoRetry(message, regIds, function(err, response) {
                if (err) console.error("ERROR:   " + JSON.stringify(err));
                else console.error("SUCCESS: " + JSON.stringify(response));
            });
        }
    }
};

exports.sendMessageWithCallback = function(regIds, notification, data, retry, times, user, result) {
    console.log("gcm.controller.js.sendMessage")
    console.log("regIds: " + JSON.stringify(regIds))
    console.log("notification: " + JSON.stringify(notification))
    console.log("data: " + JSON.stringify(data))
    console.log("retry: " + JSON.stringify(retry))
    console.log("times: " + JSON.stringify(times))
    console.log("result: " + JSON.stringify(result))
    var message = new gcm.Message();

    if (notification) {
        if (!isJson(notification)) {
            console.log("preParse")
            notification = JSON.parse(notification)
            console.log("postParse")
        }
        for (var key in notification) {
            if (notification.hasOwnProperty(key)) {
                console.log(key + " -> " + notification[key]);
                message.addNotification(key, notification[key]);
            }
        }
    }

    if (data) {
        data = JSON.parse(data)
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                console.log(key + " -> " + data[key]);
                message.addNotification(key, data[key]);
            }
        }
    }

    if (data || notification) {
        if (retry) {
            if (times) {
                sender.send(message, regIds, times, function(err, response) {
                    if (err) console.error("ERROR:   " + JSON.stringify(err));
                    else console.error("SUCCESS: " + JSON.stringify(response));
                    result(response, user);
                    //return result(err, result);
                });
            } else {
                sender.send(message, regIds, times, function(err, response) {
                    if (err) console.error("ERROR:   " + JSON.stringify(err));
                    else console.error("SUCCESS: " + JSON.stringify(response));
                    result(response, user);
                    //return result(err, result);
                });
            }
        } else {
            sender.sendNoRetry(message, regIds, function(err, response) {
                if (err) console.error("ERROR:   " + JSON.stringify(err));
                else console.error("SUCCESS: " + JSON.stringify(response));
                result(response, user);
                //return result(err, result);
            });
        }
    }
};

function isJson(_obj) {
    var _has_keys = 0;
    for (var _pr in _obj) {
        if (_obj.hasOwnProperty(_pr) && !(/^\d+$/.test(_pr))) {
            _has_keys = 1;
            break;
        }
    }

    return (_has_keys && _obj.constructor == Object && _obj.constructor != Array) ? true : false;
}