var maxMultiplicator = 4;

//TODO esta todo con PNG, hacerlo variable

function resizeImageToBiggest(name, finalName, sizes){
	console.log("gm.controller.js.resizeImageToBiggest");
	console.log(sizes)

	// obtain an image object:
	require('lwip').open("server/pictures/"+finalName, function(err, image){
		//Define el maximo posible obtenible de anchura
		var maxWidth = image.width();
		//Define el maximo posible obtenible de altura
		var maxHeight = image.height();
		var finalWidth = 0;
		var finalHeight = 0;

		//De este FOR sacamos los tamaños maximos que se necesitan (aunque luego no se puedan conseguir)
		for (var i = sizes.length - 1; i >= 0; i--) {
			if(sizes[i].width*maxMultiplicator>finalWidth) {
				finalWidth=sizes[i].width*maxMultiplicator;
			}
			if(sizes[i].height*maxMultiplicator>finalHeight) {
				finalHeight=sizes[i].height*maxMultiplicator;
			}
		};
		
		//Si necesitamos obtener una altura o anchura mayor de la maxima...
		if(finalWidth>maxWidth || finalHeight>maxHeight){
			if(finalWidth>finalHeight){
				finalHeight = finalHeight*maxWidth/finalWidth; //Primero calculamos el otro parametro que le acompañaria manteniendo el aspect ratio
				finalWidth = maxWidth; //y despues la equiparamos a la maxima 
			}else{
				finalWidth = finalWidth*maxHeight/finalHeight;
				finalHeight = maxHeight;
			}
		}

		var scaleWidth;
		var scaleHeight;

		//Obtenemos los dos posibles escalados (anchura o altura)
		scaleWidth=finalWidth/image.width();
		scaleHeight=finalHeight/image.height();

		var finalScale;
		//Determinamos el escalado que menos informacion nos lleve a perder
		if(scaleWidth>scaleHeight){
			finalScale = scaleWidth;
		}else{
			finalScale = scaleHeight;
		}
		//Necesitamos un solo valor para el escalador para que no se deforme la imagen

		console.log("About to batch");
		console.log("Scale: " + finalScale);
		console.log("Crop: " + finalWidth + " | " + finalHeight);

		// check err...
		// define a batch of manipulations and save to disk as JPEG:
		image.batch()
			.scale(finalScale) 					//Escalamos
		    .crop(finalWidth, finalHeight)      // cropeamos (salvo error mio, aqui ya solo haria crop de verdad de altura o anchura, vamos que solo cortaria en un sentido)
		    /*.toBuffer('png', function(err, buffer){
		    	console.log("Buffered");
		    	console.log(buffer.toString('base64'))
		    });*/
		    .writeFile("server/pictures/"+finalName, function(err){ //Guardamos la imagen
		      // check err...
		      // done.
		    });
	});
}

//Deprecated
//Potencialmente lleno de fallos
exports.getImageResized = function(fullName, width, height, multiplier, done){
	console.log("gm.controller.js.getImageResized");
	console.log(fullName + " " + width + " " + height + " " + multiplier + " " + done)
	// obtain an image object:

	require('lwip').open(fullName, function(err, image){
		var scaleWidth;
		var scaleHeight;
		var type = 0;

		if (width && height){
			width = width*multiplier;
			height = height*multiplier;

			scaleWidth=width/image.width();
			scaleHeight=height/image.height();

			var finalScale;
			if(scaleWidth<scaleHeight){
				finalScale = scaleWidth;
			}else{
				finalScale = scaleHeight;
			}
		
		}else{
			finalScale = multiplier;
		}

	    // check err...
	    // define a batch of manipulations and save to disk as JPEG:
	    image.batch()
		    .scale(finalScale)
		    .toBuffer('png', function(err, buffer){
			   	console.log("SUCCESS Buffered");
			  	//done(false, buffer.toString('base64'));
			  	done(false, buffer);
			});
			/*.writeFile(fullName+"edited"+Math.random()+".png", function(err){ //Guardamos la imagen
			    // check err...
			    // done.
			});*/
	});
}

exports.saveImage = function(name, base64Data, sizes, done) {
	console.log("gm.controller.js.saveImage");
	console.log(sizes)
	var finalName = ""+new Date()+""+Math.random()+name;

	//Primero guardamos la imagen
	require("fs").writeFile("server/pictures/"+finalName, base64Data, 'base64', function(err) {
	  if(err){
	  	console.log("ERROR saving image")
	  	done(true, err);
	  }else{
	  	console.log("SUCCESS saving image")
	  	//Ahora que se ha guardado, podemos manipularla
	  	resizeImageToBiggest(name, finalName, sizes);
	  	//Y dar respuesta de que todo ha ido bien
	  	done(false, "server/pictures/"+finalName);
	  }
	});
};