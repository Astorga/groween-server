'use strict';

angular.module('groweenApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/:id', {
        templateUrl: 'app/rate/rate.html',
        controller: 'RateCtrl'
      });
  });
