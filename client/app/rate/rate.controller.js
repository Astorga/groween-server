'use strict';

angular.module('groweenApp')
  .controller('RateCtrl', function ($scope, $routeParams, $http, $location) {

    $location.path('/rate/' + $routeParams.id ).replace();

  });

angular.module('groweenApp')
  .controller('RateHomeCtrl', function ($scope, $routeParams, $http, $location) {
    $scope.pageClass = 'page-home';
    if ($routeParams && $routeParams.id) {
      $scope.state = 0;
      $http.get('api/questionaries/' +  $routeParams.id + '/public')
        .success(function(data) {
          $scope.questionary = data;
          $scope.answers = [];
          $scope.inexistent = false;
        })
        .error(function(data, status, headers, config) {
          $scope.inexistent = true;
        });
    }

    $scope.start = function() {
      $location.path('/rate/' + $routeParams.id + '/form');
      $scope.$apply();
    };

  });

angular.module('groweenApp')
  .controller('RateFormCtrl', function ($scope, $routeParams, $http, $location, smoothScroll) {
    $scope.pageClass = 'page-form';
    $scope.selected = -1;
    if ($routeParams && $routeParams.id) {
      $http.get('api/questionaries/' +  $routeParams.id + '/public')
        .success(function(data) {
          $scope.questionary = data;
          $scope.rates = [];
          $scope.total = $scope.questionary.questions.length;
        })
        .error(function(data, status, headers, config) {
          $location.path('/rate/' + $routeParams.id );
          $scope.$apply();
        });
      $http.get('api/ngos/actives')
        .success(function(data) {
          $scope.ngos = data;
        });
    }

    $scope.rated = function(question, value) {
      $scope.rates[question] = value;
      //Scroll to next
//            alert('Rated question' + question + ' with value ' + value);

      var element;
      if (question < $scope.total) {
        var next = question;
        for (var i = next; i <= $scope.total; i++) {
          next = i;
          if (!$scope.rates[next]) {
            break;
          }
        }

        if (next >= $scope.total) {
          element = document.getElementById('ID_send');

        } else {
          element = document.getElementById('ID_' + next);
        }
      } else {
        element = document.getElementById('ID_send');
      }
      smoothScroll(element);
    };

    $scope.sendRates = function() {
      for (var i = 0; i < $scope.total; i++) {
        if (!$scope.rates[i]) {
          alert('Tienes que responder a todas las preguntas');
          var element = document.getElementById('ID_' + (i));
          smoothScroll(element);
          return;
        }
      }


      if ($scope.selected < 0) {
        var random = getRandomInt(0, $scope.ngos.length);
        if (random < 0 || random >= $scope.ngos.length) {
          random = $scope.ngos.length - 1;
        }
        $scope.selected = $scope.ngos[random]._id;
      }

      //Send data
      var post = {
        questionary: $scope.questionary._id,
        answers: $scope.rates,
        comment: $scope.comment,
        ngo: $scope.selected
      };

      $http.post('api/questionaryAnswers', post)
        .success(function(data) {
          //go to next step
          var ngo = null;
          for (var i = 0; i < $scope.ngos.length; i++) {
            ngo = $scope.ngos[i];
            if (ngo._id == $scope.selected) {
              break;
            }
          }
          var url = '/rate/' + $scope.questionary._id + '/last/' + ngo.name + '/' + data.price;
          $location.path(url).replace();
          return;
        })
        .error(function(data, status, headers, config) {
          alert('Error al crear la valoración\n' + status + '\n' + data);
        });
    };

  });

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

angular.module('groweenApp')
  .controller('RateLastCtrl', function ($scope, $routeParams, $http) {
    $scope.pageClass = 'page-last';
    if ($routeParams) {
      $scope.selectedOng = $routeParams.selectedOng;
      $scope.yourDonation = $routeParams.yourDonation;
    }

    $http.get('api/ngos/totalDonation').success(function(data) {
      $scope.donations = data.totalDonation;
    });

  });
