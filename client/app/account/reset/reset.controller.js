'use strict';

angular.module('groweenApp')
  .controller('ResetCtrl', function ($scope, Auth, $location, $window, $http) {
    var email = $location.$$search.email;
    var token = $location.$$search.token;
    if (!email || !token) {
      window.location.href = '/';
    }

    $scope.ios = /iPad|iPhone|iPod/.test( navigator.userAgent );

    if ($scope.ios) {
      window.location.href = 'groween://resetPassword?email=' + email + '&token=' + token;
    };

    $scope.user = {};
    $scope.errors = {};

    $scope.reset = function(form) {
      $scope.submitted = true;
      if (!$scope.password || $scope.password.length == 0) {
        $scope.passwordRequired = true;
        return;
      }
      $scope.passwordRequired = false;

      if (!$scope.repassword ||$scope.repassword.length == 0) {
        $scope.repasswordRequired = true;
        return;
      }
      $scope.repasswordRequired = false;

      if ($scope.repassword != $scope.password) {
        $scope.repasswordSame = true;
        return;
      }
      $scope.repasswordSame = false;

      if(form.$valid) {
        var post = { email: email, refreshPasswordToken: token, newPassword: $scope.password };
        alert(JSON.stringify(post));
        $http.post('/api/users/resetPassword', post).
          success(function(data, status, headers, config) {
            alert('The password has benn changed correctly!!!');
            window.location.href = '/';
          }).
          error(function(data, status, headers, config) {
            alert('Error changing the password');
          });
      }
    };

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
