'use strict';

describe('Directive: ngo', function () {

  // load the directive's module and view
  beforeEach(module('groweenApp'));
  beforeEach(module('app/admin/ngos/ngo/ngo.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ngo></ngo>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the ngo directive');
  }));
});
