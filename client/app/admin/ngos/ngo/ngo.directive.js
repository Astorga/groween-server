'use strict';

angular.module('groweenApp')
  .directive('ngoDir', function ($http, $cookieStore) {
    return {
      templateUrl: 'app/admin/ngos/ngo/ngo.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        if (scope.ngo.image) {
          scope.ngo.imageUrl = "/api/images/" + scope.ngo.image._id + "?name='thumbnail'&multiplier=2&access_token=" + $cookieStore.get('token');
        }
        scope.ngoNameChanged = function (data, id) {
          return $http.put('/api/ngos/' + id, {name: data});
        };

        scope.ngoDescriptionChanged = function (data, id) {
          return $http.put('/api/ngos/' + id, {description: data});
        };

        scope.ngoUrlChanged = function (data, id) {
          return $http.put('/api/ngos/' + id, {url: data});
        };

        scope.deleteImage = function () {
          $http.delete('/api/images/' + scope.ngo.image._id)
            .success(function(data, status, headers, config) {
              $http.put('/api/ngos/' + scope.ngo._id, {image: null})
                .success(function(data, status, headers, config) {
                  scope.ngo = data;
                })
                .error(function(data, status, headers, config) {
                  alert('Error storing image in ngo\n' + status + '\n' + data);
                });
            })
        }

        scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
          var post = {
            name: fileObj.filename,
            base64: fileObj.base64,
            images: [
              {
                "name": "thumbnail",
                "width": 80,
                "height": 80
              }
            ]
          };
          $http.post('/api/images', post)
            .success(function(data, status, headers, config) {
              $http.put('/api/ngos/' + scope.ngo._id, {image: data._id})
                .success(function(data, status, headers, config) {
                  scope.ngo = data;
                  scope.image64 = null;
                  scope.ngo.imageUrl = "/api/images/" + scope.ngo.image._id + "?name='thumbnail'&multiplier=2&access_token=" + $cookieStore.get('token');
                })
                .error(function(data, status, headers, config) {
                  alert('Error storing image in ngo\n' + status + '\n' + data);
                  scope.image64 = null;
                });

            })
            .error(function(data, status, headers, config) {
              alert('Error creating image\n' + status + '\n' + data);
              scope.image64 = null;
            });
        };
      }
    };
  });
