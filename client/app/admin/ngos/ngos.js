'use strict';

angular.module('groweenApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin/ngos', {
        templateUrl: 'app/admin/ngos/ngos.html',
        controller: 'NgosCtrl'
      });
  });
