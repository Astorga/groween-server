'use strict';

describe('Controller: NgosCtrl', function () {

  // load the controller's module
  beforeEach(module('groweenApp'));

  var NgosCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NgosCtrl = $controller('NgosCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
