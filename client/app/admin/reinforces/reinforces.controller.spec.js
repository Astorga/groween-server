'use strict';

describe('Controller: ReinforcesCtrl', function () {

  // load the controller's module
  beforeEach(module('groweenApp'));

  var ReinforcesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReinforcesCtrl = $controller('ReinforcesCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
