'use strict';

angular.module('groweenApp')
  .controller('TemplatesCtrl', function ($scope, $http) {
    $scope.professions = [{_id: -1, name: 'Todas las profesiones'}];

    $http.get('/api/professions').success(function(data) {
      $scope.professions = $scope.professions.concat(data);
      $scope.changedProfession();
    });

    $scope.areas = [];
    $scope.selectedCategory = undefined;

    $scope.oneAtATime = false;

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };

    $scope.categorySelected = function(category) {
      $scope.selectedCategory = category;
    };

    $scope.integralSelected = function() {
      $scope.selectedCategory = 'integral';
    }

    $scope.changedProfession = function() {
      var url = '/api/areas';
      if ($scope.selectedProfession._id != -1) {
        url += '?profession=' + $scope.selectedProfession._id + '&emptyProfession=false';
      } else {
        url += '?profession=none'
      }
      $http.get(url).success(function(areas) {
        $scope.areas = areas;
      });
    };
  });
