'use strict';

angular.module('groweenApp')
  .directive('templateDir', function ($http) {
    return {
      templateUrl: 'app/admin/templates/template/template.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.visible = false;
        scope.templates = [];
        scope.selectedCategory = undefined;
        scope.$watch(attrs.category, function (newCategory) {
          if (newCategory) {
            if (typeof newCategory == 'string') {
              $http.get('api/templates/noCategory').
                success(function(data, status, headers, config) {
                  scope.visible = true;
                  scope.selectedCategory = newCategory;
                  scope.templates = data;
                }).
                error(function(data, status, headers, config) {
                  alert('Error loading category templates\n' + status + '\n' + data);
                });
              return;
            }
            $http.get('api/categories/' + newCategory._id + '/templates').
              success(function(data, status, headers, config) {
                scope.visible = true;
                scope.selectedCategory = newCategory;
                scope.templates = data;
              }).
              error(function(data, status, headers, config) {
                alert('Error loading category templates\n' + status + '\n' + data);
              });
          }
        });

        scope.addTemplate = function() {
          if(!scope.newTemplate || scope.newTemplate === '') {
            return;
          }
          var post = { name: scope.newTemplate, category: scope.selectedCategory._id};
          $http.post('/api/templates/', post).
            success(function(data, status, headers, config) {
              scope.newTemplate = '';
              scope.templates.push(data);
            }).
            error(function(data, status, headers, config) {
              alert('Error creating template\n' + status + '\n' + data);
            });
        };

        scope.templateNameChanged = function(data, id) {
          return $http.put('/api/templates/' + id, {name: data});
        };

        scope.templateDescriptionChanged = function(data, id) {
          return $http.put('/api/templates/' + id, {description: data});
        };

        scope.deleteTemplate = function(templateId, index) {
          var deleteConfirm = confirm('All topics and questions are going to be deleted\n\nAre you absolutely sure you want to delete?');

          if (deleteConfirm) {
            if (templateId) {
              $http.delete('/api/templates/' + templateId);
              scope.templates.splice(index, 1);
            }
          }
        };

        scope.questionsChangedInTemplate = function(template, questions) {
          alert('questionsChangedInTemplate');
          for (var i = 0; i < scope.templates; i++) {
            if (scope.templates[i]._id == template._id) {
              scope.templates[i].questions = questions;
              break;
            }
          }
        };

        scope.activeChanged = function(index) {
          var template = scope.templates[index];
          if (template.active) {
            //Check
            alert('Questions: ' + template.questions.length);
            if (template.questions.length != 10) {
              alert('The template must have 10 questions');
              template.active = false;
              return;
            }
          }
          $http.put('/api/templates/' + template._id, {active: template.active});
        };

      }
    };
  });
