'use strict';

angular.module('groweenApp')
  .directive('questionsDir', function ($http) {
    return {
      templateUrl: 'app/admin/templates/template/questions/questions.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.topics = scope.selectedCategory.topics;
        scope.sortableOptions = {
          stop: function(e, ui) {
            // this callback has the changed model
            var questionsIds = [];
            for (var i = 0; i < scope.questions.length; i++) {
              questionsIds[i] = scope.questions[i]._id;
            }
            $http.put('/api/templates/' + scope.template._id, {questions: questionsIds}).
              success(function(data, status, headers, config) {
                return true;
              }).
              error(function(data, status, headers, config) {
                alert('Error ordering questions\n' + status + '\n' + JSON.stringify(data));
                return false;
              });
          },
          handle: ' .handle',
          axis: 'y'
        };
        scope.questions = scope.template.questions;
        scope.newTopic = undefined;

        scope.addQuestion = function() {
          if(!scope.newTopic || scope.newTopic === '') {
            alert('You must select a topic');
            return;
          }
          if(!scope.newQuestion || scope.newQuestion === '') {
            alert('You must add question text');
            return;
          }
          var topicName = scope.newTopic.name;
          if (!topicName)
            topicName = scope.newTopic;
          $http.post('api/categories/' + scope.selectedCategory._id + '/topics', {name: topicName}).success(function(data) {
            var topic = data;
            var insert = true;
            scope.topics.forEach(function(storedTopic) {
              if (storedTopic.name == topic.name)
              insert = false;
            });
            if (insert) {
              scope.topics.push(topic);
            }

            var post = { name: scope.newQuestion, topic: topic._id };
            $http.post('/api/templates/' + scope.template._id + '/questions', post).
              success(function(data, status, headers, config) {
                scope.newQuestion = '';
                scope.newTopic = '';
                scope.questions.push(data);
              }).
              error(function(data, status, headers, config) {
                alert('Error creating question\n' + status + '\n' + data);
              });
          });
        };

        scope.questionNameChanged = function(data, id) {
          return $http.put('/api/questions/' + id, {name: data});
        };

        scope.questionTopicChanged = function(data, id) {
          return $http.put('/api/questions/' + id, {topic: data});
        };

        scope.deleteQuestion = function(objectId, index) {
          var deleteConfirm = confirm('Are you absolutely sure you want to delete?');

          if (deleteConfirm) {
            if (objectId) {
              $http.delete('/api/questions/' + objectId);
              scope.questions.splice(index, 1);
            }
          }
        };
      }
    };
  });
