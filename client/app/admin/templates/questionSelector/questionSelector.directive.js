'use strict';

angular.module('groweenApp')
  .directive('questionSelector', function () {
    return {
      templateUrl: 'app/admin/templates/questionSelector/questionSelector.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });