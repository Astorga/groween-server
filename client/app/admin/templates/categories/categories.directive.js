'use strict';

angular.module('groweenApp')
  .directive('categoriesDir', function ($http) {
    return {
      templateUrl: 'app/admin/templates/categories/categories.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.categories = [];
        if (scope.area) {
          scope.categories = scope.area.categories;
        }

        if (scope.area.name == 'ESTUDIOS INTEGRALES') {
          scope.integral = true;
        } else {
          scope.integral = false;
        }

        scope.addCategory = function() {
          if(!scope.newCategory || scope.newCategory === '') {
            return;
          }
          var post = { name: scope.newCategory };
          if (scope.selectedProfession._id != -1) {
            post.profession = scope.selectedProfession._id;
          }
          $http.post('/api/areas/' + scope.area._id + '/categories', post).
            success(function(data, status, headers, config) {
              scope.newCategory = '';
              scope.categories.push(data);
            }).
            error(function(data, status, headers, config) {
              alert('Error creating category\n' + status + '\n' + data);
            });
        };

        scope.categoryNameChanged = function(data, id) {
          return $http.put('/api/categories/' + id, {name: data});
        };


        scope.deleteCategory = function(categoryId, index) {
          var deleteConfirm = confirm('All topics (and questions) and templates are going to be deleted\n\nAre you absolutely sure you want to delete?');

          if (deleteConfirm) {
            $http.delete('/api/categories/' + categoryId);
            scope.categories.splice(index, 1);
          }
        };
      }
    };
  });
