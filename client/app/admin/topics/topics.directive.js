'use strict';

angular.module('groweenApp')
  .directive('topics', function () {
    return {
      templateUrl: 'app/admin/topics/topics.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {

      }
    };
  });
