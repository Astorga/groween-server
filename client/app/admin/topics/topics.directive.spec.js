'use strict';

describe('Directive: topics', function () {

  // load the directive's module and view
  beforeEach(module('groweenApp'));
  beforeEach(module('app/admin/topics/topics.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<topics></topics>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the topics directive');
  }));
});